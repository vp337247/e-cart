const products = [
    {
      name: "PlayStation 5",
      imageUrl:
        "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAH4AvAMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQIFBgcDBAj/xABDEAACAQICBAoGCAMJAQAAAAAAAQIDEQQFBhIhMQcTFSIyQVFxlNIUIzNVYYEWF0JGVnLB4mKRoTQ2Q0VkdKSx0ST/xAAWAQEBAQAAAAAAAAAAAAAAAAAAAQL/xAAVEQEBAAAAAAAAAAAAAAAAAAAAEf/aAAwDAQACEQMRAD8A4wADQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWpU6larClRhKdSclGMY75N7EkBUGWeRKOypnGTxmtjj6U3Z96i1/JjkSHvvJvES8oGJBluRI++sm8TLyjkSPvnJ/Ey8oGJBleRI++cn8S/KTyJH3zk3iX5QMSDLchr3zk3in5SORF75yfxT8oGKBleRF74yfxT8o5Fj74yjxL8oGKBleRE/84yfxT8p8eOwNXA1VTqypzUo60KlGopwqR3XjJb9qfdZgfMAAAAAAAAAAAAAH0Ze3HG0WnZqW9dzPnPbBP8A+ql3geEeiu4krHoruLAAAAIJAAAAAAAPao36Lh1fYnPZ2bjxPSf9npfCUv0A8wAAAAAAAAAAAAA9MK7Ymn+Y8y9DZXpv+JAeceiu4kiPRXcSAAAAAAQSAAIJAEHpL2MO+X6FCz9lHvYFQAAAAAAAAAABBIAtS9rDvRUtT9pD8yAotyJIW4kAAAAAAAAAAABZ9Bd7Kk/YXe/0AgAAAAAIJIAkAAAAALU/aQ/MivWbnwW6Kx0kzyVXGU3LLsHHXq70qkn0YX/q/ggNLW5EnfNLsp0W0dy+lKlo9ldTFV52pQq0r81dKTSab3pfNGpV5ZPj6NN0tF8mjOinKs4wqQVm7bNWe2y337RRzAHT5YbR6NWVV6MZf6JNSVH1lVzb3JS59r9ezqaCwWTa6w30Xy2WLcrx9ZWUHGyatz7ty2W3f12KOYA6ZGjkNTUrU9GsueGivXSbq63evWWSu0t77fgHRyWnG9XRnKVx7tQlr1dSKfXJ693q3SaS+N0KOZg6ZHDZRKXEx0Xy2Nam7101WvZpbIx4zbZXe1q6I1ckUZ4j6MZTLDxsoxi6rd93OevsvZtWT7BRzQHSJ0MthHifo3knpDTlGo+OVOSTa5vPvJuzSvbb2kyp5ZCrN0tGso9Q5KUZQqublFuzceMslsSau/mKObXJ/wANd7/Q6ngc1ypZnh5ZpovkEcrq1NSdXD0JOVJNbLtv7Oy+xdfYbHpzwcZbjMorPI8DQw2OpespOjHVVb+F22O63PtFHCAHGUZOM4uMk7NSVmn8QAAAAAAAAAAAFqcJ1akKdOLnOclGMVvk3sSP03oJo3HRrRzC4Fxj6TJcZipR66rW3+W5fBH500czZZHneEzN4Sli5YaevGlVbScup3XWt6Olrhwq2/u9Tv8A7x+UDoWkWhuF0ixtPE4vHYulxcNSNOk46qV7ventf/h8z4OsslVlN4zGalpqnSTjq09a9rbOpu6v1rbc0dcOdZfd2n4x+QlcOtf8OU/GPyEG7Lg3y+1aXKOPdatPWlWcoa1mpKUejuett7kUqcHOWUsveE9OxzU6sajm3T1nZNW6O7nM0369q/4cpeMfkKz4cqs7X0dp7OzGPyBW2fV3l/MvmeZPi1aF50+ar3suZ2tv5kvg9wGvUnynmWtUvryU6d5X335hqdDhrdWtThVySnQhJpSqvEymoLt1VC7+RlfrYyzrxOD8PiPKBk1wcZbGm6UcxzJU3LWcFOnZvt6HxPoraCZfU9Gk8bj4zw0FCM4yp6zSk2rvU37bdyRgqnC3l0Kc5Qq4SpJJtQVGvFy+F3GyMT9d0/w7Hxn7AN8XB5lmvGpDG42KcUqkU4Wq9rlzd76yZ8H+Vyp1KLrVuInKUo0lTpJU5Pc4tRvs7++5o8OHScYqP0djs/1r8hP16Tf3dj4z9gRty4NsohRlBVazm5qSqOnScoq1rK8bW6/kbJg8F6Fl9DCcdOsqEdWNSaSlZblsSWxbN3UcrfDlL8Orxn7Cr4b7/d3/AJn7AMZwxaI8nY3l/AU0sJipauJjFdCq/td0v++85odRzfhdpZrlmKwFfRyEqeIpypyVTFtrb17Ip/E5d87lAAAAAAAAAAAAAAAAAAAACAJAAAAAAAAAAAAAACAP/9k=",
      description:
        "PlayStation 5 (PS5) is a home video game console developed by Sony Interactive Entertainment. Announced in 2019 as the successor to the PlayStation 4, the PS5 was released on November 12, 2020 in Australia, Japan, New Zealand, North America, Singapore, and South Korea, and November 19, 2020 onwards in other major markets except China and India.",
      price: 499,
      countInStock: 15,
    },
    /*{
      name: "Iphone 12",
      imageUrl:
        "https://images.unsplash.com/photo-1605787020600-b9ebd5df1d07?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1463&q=80",
      description:
        "Welcome to a new era of iPhone. Beautifully bright 6.1-inch Super Retina XDR display.1 Ceramic Shield with 4x better drop performance.2 Incredible low-light photography with Night mode on all cameras. Cinema-grade Dolby Vision video recording, editing, and playback. Powerful A14 Bionic chip. And new MagSafe accessories for easy attach and faster wireless charging.3 Let the fun begin.",
      price: 1099,
      countInStock: 10,
    },
    {
      name: "Cannon EOS-1D",
      imageUrl:
        "https://images.unsplash.com/photo-1519183071298-a2962feb14f4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
      description:
        "The EOS-1D X combines speed with image quality, to create the next generation camera for professionals. Full frame 18 megapixel sensor with Dual “DIGIC 5+” processors sets the standard, and up to 12 frames per second shooting takes it beyond.",
      price: 1300,
      countInStock: 5,
    },
    {
      name: "Amazon Alexa",
      imageUrl:
        "https://images.unsplash.com/photo-1518444065439-e933c06ce9cd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1267&q=80",
      description:
        "It is capable of voice interaction, music playback, making to-do lists, setting alarms, streaming podcasts, playing audiobooks, and providing weather, traffic, sports, and other real-time information, such as news. Alexa can also control several smart devices using itself as a home automation system.",
      price: 50,
      countInStock: 25,
    },
    {
      name: "Audio Technica Headphones",
      imageUrl:
        "https://images.unsplash.com/photo-1558756520-22cfe5d382ca?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
      description:
        "Outfitted with 45mm large-aperture dynamic drivers and an over-ear, closed-back design, the ATH-M50x headphones deliver clarity, deep bass, and extended bandwidth (15 Hz to 28 kHz) while isolating you from outside sounds.",
      price: 233,
      countInStock: 4,
    },
    {
      name: "JBL FLIP 4",
      imageUrl:
        "https://images.unsplash.com/photo-1564424224827-cd24b8915874?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1489&q=80",
      description:
        "JBL Flip 4 is the next generation in the award-winning Flip series; it is a portable Bluetooth speaker that delivers surprisingly powerful stereo sound. This compact speaker is powered by a 3000mAh rechargeable Li-ion battery that offers up to 12 hours of continuous, high-quality audio playtime.",
      price: 140,
      countInStock: 10,
    },
    {
      name: "PlayStation 5",
      imageUrl:
        "https://images.unsplash.com/photo-1606813907291-d86efa9b94db?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80",
      description:
        "PlayStation 5 (PS5) is a home video game console developed by Sony Interactive Entertainment. Announced in 2019 as the successor to the PlayStation 4, the PS5 was released on November 12, 2020 in Australia, Japan, New Zealand, North America, Singapore, and South Korea, and November 19, 2020 onwards in other major markets except China and India.",
      price: 499,
      countInStock: 15,
    },
    {
      name: "PlayStation 5",
      imageUrl:
        "https://images.unsplash.com/photo-1606813907291-d86efa9b94db?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80",
      description:
        "PlayStation 5 (PS5) is a home video game console developed by Sony Interactive Entertainment. Announced in 2019 as the successor to the PlayStation 4, the PS5 was released on November 12, 2020 in Australia, Japan, New Zealand, North America, Singapore, and South Korea, and November 19, 2020 onwards in other major markets except China and India.",
      price: 499,
      countInStock: 15,
    },*/
  ];
  
  module.exports = products;