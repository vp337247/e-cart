const bcrypt = require('bcrypt')
const User = require("../models/User");


const signupController = async (req, res) => {
  const { fullName, email, password } = req.body;

  try {

    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.status(409).json({ message: 'Email already exists' });
    }

    // Validate password
    const passwordRegex = /^(?=.*[@#$%&]).{6,}$/;
    if (!passwordRegex.test(password)) {
      return res.status(400).json({
        message:
          'Password must be at least 6 characters long and contain at least one of the following special characters: @, #, $, %, &',
      });
    }
    
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = new User({ fullName, email, password: hashedPassword });
    // Additional code for user creation or other operations
    user.save((err) => {
      if (err) {
        console.error('Error saving user:', err);
        res.status(500).json({ message: 'Error saving user' });
      } else {
        console.log('User saved successfully');
        res.status(200).json({ message: 'User saved successfully' });
      }
    })
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error creating user" });
  }
  
};


const loginController = async (req, res) => {
  const { email, password } = req.body

  try {
    const user = await User.findOne({ email });
    if (!user) {
      res.status(404).json({ message: 'User not found' });
      return;
    }

    const isPasswordMatch = await bcrypt.compare(password, user.password);
    
    if (!isPasswordMatch) {
      res.status(401).json({ message: 'Invalid password' });
      return;
    }

    res.status(200).json({ message: 'Login successful', fullName: user.fullName });

  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error during login' });
  }
};

module.exports = { signupController, loginController }