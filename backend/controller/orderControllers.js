const Order = require('../models/Order');

// Create a new order
const createOrder = async (req, res) => {
  try {
    const { fullName, address, city, postalCode, country, items, subtotal } = req.body;

    // Create a new order object
    const order = new Order({
      fullName,
      address,
      city,
      postalCode,
      country,
      items,
      subtotal,
    });

    // Save the order to the database
    const createdOrder = await order.save();
    
    res.status(201).json(createdOrder);
  } catch (error) {
    console.error('Error creating order:', error);
    res.status(500).json({ message: 'Error creating order' });
  }
};


// Get all orders
const getOrders = async (req, res) => {
  try {
    const orders = await Order.find({});
    res.json(orders);
  } catch (error) {
    console.error('Error getting orders:', error);
    res.status(500).json({ message: 'Error getting orders' });
  }
};

// Get an order by ID
const getOrderById = async (req, res) => {
  try {
    const order = await Order.findById(req.params.id);

    if (!order) {
      res.status(404).json({ message: 'Order not found' });
      return;
    }

    res.json(order);
  } catch (error) {
    console.error('Error getting order:', error);
    res.status(500).json({ message: 'Error getting order' });
  }
};

module.exports = {
  createOrder,
  getOrders,
  getOrderById,
};
