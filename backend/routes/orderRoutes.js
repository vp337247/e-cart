const express = require('express');
const router = express.Router();
const {
  createOrder,
  getOrders,
  getOrderById,
} = require('../controller/orderControllers');

router.post('/', createOrder); // Add this route for placing an order
router.get('/', getOrders);
router.get('/:id', getOrderById);

module.exports = router;
