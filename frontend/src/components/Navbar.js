import '../sass/Navbar.scss'
import { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

function Navbar({ click }) {
    const cart = useSelector((state) => state.cart);
    const { cartItems } = cart;

    const [fullName, setFullName] = useState('');

    /*useEffect(() => {
        // Retrieve the user's full name from localStorage
        const storedFullName = localStorage.getItem('fullName');
        setFullName(storedFullName);
    }, []);*/

    const navigate = useNavigate(); 
    const handleLogout = () => {
        // Remove the full name from localStorage and navigate to the homepage
        localStorage.removeItem('fullName');
        navigate('/'); 
    };

    const [showLogout, setShowLogout] = useState(false);

    useEffect(() => {
        const storedFullName = localStorage.getItem('fullName');
        setFullName(storedFullName);
    }, [fullName]);

    const getCartCount = () => {
        return cartItems.reduce((qty, item) => qty + Number(item.qty), 0);
    };

 
    return (
        <nav className="navbar">
            {/*logo*/}
            <div className="navbar__logo">
                <h2>Winkel</h2>
            </div>
            {/*links*/}
            <ul className="navbar__links">
                {fullName && (
                    <>
                        <li>
                            <Link to="/cart" className="cart__link">
                                <i className="fas fa-shopping-cart"></i>
                                Cart
                                <span className="cartlogo__badge">{getCartCount()}</span>
                            </Link>
                        </li>
                        <li
                            className="fullName"
                            onMouseEnter={() => setShowLogout(true)}
                            onMouseLeave={() => setShowLogout(false)}
                        >
                            <div className="dropdown">
                                <button className="dropbtn">{fullName}</button>
                                {showLogout && (
                                    <div className="dropdown-content">
                                        <button onClick={handleLogout}>Logout</button>
                                    </div>
                                )}
                            </div>
                        </li>
                    </>
                )}
                {!fullName && (
                    <>
                        <li>
                            <Link to="" className="cart__link" onClick={(e) => e.preventDefault()}>
                                <i className="fas fa-shopping-cart"></i>
                                Cart
                                <span className="cartlogo__badge">0</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/login" className='cart__link'>
                                <i className="far fa-user"></i><span className=''>Sign in</span>
                            </Link>
                        </li>
                    </>
                )}
            </ul>

            {/*hamburger menu*/}
            <div className="hamburger__menu" onClick={click}>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </nav>
    );
}

export default Navbar;