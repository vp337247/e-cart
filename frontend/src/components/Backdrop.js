import React from 'react'

import "../sass/Backdrop.scss"

function Backdrop({ show, click }) {
  return (
    show && <div className="backdrop" onClick={click}>

    </div>
  )
}

export default Backdrop
