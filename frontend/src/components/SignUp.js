import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Modal from 'react-modal';
import axios from 'axios';


import '../sass/SignUp.scss';

Modal.setAppElement('#root');
function SignUp() {

  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [errorMessage, setErrorMessage] = useState(''); // State variable for error message
  const [isModalOpen, setIsModalOpen] = useState(false);
  const navigate = useNavigate();


  const handleSignUp = async (e) => {
    e.preventDefault();

    try {
      // Send signup data to the backend server using Axios
      const response = await axios.post('http://localhost:5000/api/signup', {
        fullName,
        email,
        password,
      });

      if (response.status === 200) {
        console.log('User signed up successfully');

        // Redirect to login page
        navigate("/");
      }
    } catch (error) {
      if (error.response.status === 409) {
        setErrorMessage('Email already exists');
      } else if (error.response.status === 400) {
        setErrorMessage(
          'Password must be at least 6 characters long and contain at least one of the following special characters: @, #, $, %, &'
        );
      } else {
        setErrorMessage('Error signing up');
      }
      setIsModalOpen(true);
    }
  };

  return (
    <div className="signup">
      <div className="form-popup">
        <h1>Sign Up</h1>
        <form action='/api/signup' method='post' onSubmit={handleSignUp} className="form-container">
          <label htmlFor="name"><b>Full Name</b></label>
          <input
            type="text"
            placeholder="Enter Full Name"
            name="fname"
            value={fullName}
            onChange={(e) => setFullName(e.target.value)}
            required
          />

          <label htmlFor="email"><b>Email</b></label>
          <input
            type="text"
            placeholder="Enter Email"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />

          <label htmlFor="psw"><b>Password</b></label>
          <input
            type="password"
            placeholder="Enter Password"
            name="psw"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <button type="submit" className="btn" >Sign Up</button>
          
          {/* Modal for error message */}  
          <Modal isOpen={isModalOpen} className='modal-content' onRequestClose={() => setIsModalOpen(false)}>
            <h2>Error</h2>
            <p>{errorMessage}</p>
            <button onClick={() => setIsModalOpen(false)}>Close</button>
          </Modal>
        
        </form>
        <p>Already have an account?<Link to="/login">Please Login</Link></p>
      </div>
    </div>
  );
}

export default SignUp;
