import React from 'react'
import axios from 'axios';
import { useState } from 'react';

import '../sass/CartScreen.scss'

import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

//Components
import CartItem from '../components/CartItem'

//Actions
import { addToCart, removeFromCart } from '../redux/action/cartActions'

function CartScreen() {

    const dispatch = useDispatch()

    const cart = useSelector(state => state.cart)
    const { cartItems } = cart


    const qtyChangeHandler = (id, qty) => {
        dispatch(addToCart(id, qty))
    }

    const removeHandler = (id) => {
        dispatch(removeFromCart(id))
    }

    const getCartCount = () => {
        return cartItems.reduce((qty, item) => Number(item.qty) + qty, 0)
    }

    const getCartSubTotal = () => {
        return cartItems.reduce((price, item) => (item.price * item.qty) + price, 0)
    }


    /*const razorpay = new Razorpay({
        key_id: 'your_key_id',
        key_secret: 'your_key_secret',
    });*/



    return (
        <div className='cartscreen'>
            <div className="cartscreen__left">
                <h2>Cart</h2>
                {cartItems.length === 0 ? (
                    <div>
                        <h2>Your Cart is Empty</h2><Link to="/"><h2>Go Back</h2></Link>
                    </div>
                ) : cartItems.map(item => (
                    <CartItem
                        key={item.product}
                        item={item}
                        qtyChangeHandler={qtyChangeHandler}
                        removeHandler={removeHandler} />
                ))}
            </div>
            <div className="cartscreen__right">
                <div className="cartscreen__info">
                    <p>Subtotal ({getCartCount()}) items</p>
                    <p>${getCartSubTotal().toFixed(2)}</p>
                </div>
                <div>
                    <Link to="/checkout">
                        <button>Proceed To Checkout</button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default CartScreen
