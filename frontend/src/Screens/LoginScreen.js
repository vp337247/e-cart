import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Modal from 'react-modal'
import '../sass/LoginScreen.scss';

import axios from 'axios';

function LoginScreen() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const [errorMessage, setErrorMessage] = useState(''); // State variable for error message
  const [isModalOpen, setIsModalOpen] = useState(false);

  const loginHandler = async (e) => {
    e.preventDefault();

    try {
      // Send login request to the backend server using Axios
      const response = await axios.post('http://localhost:5000/api/login', {
        email,
        password,
      });

      if (response.status === 200) {
        console.log('User logged in successfully');

        // Store the user's full name in localStorage
        localStorage.setItem('fullName', response.data.fullName);

        // Redirect to homepage
        navigate('/');
      }
    } catch (error) {
      if (error.response.status === 404) {
        setErrorMessage('User Not Found');
      } else if (error.response.status === 401) {
        setErrorMessage(
          'Invalid Password'
        );
      } else {
        console.error('Error logging in', error);
      }
      setIsModalOpen(true);
    }
  };

  return (
    <div className="loginscreen">
      <div className="form-popup">
        <h1>Log In</h1>
        <form action='/login' method='post' className="form-container" onSubmit={loginHandler}>
          <label htmlFor="email">
            <b>Email</b>
          </label>
          <input
            type="text"
            placeholder="Enter Email"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />

          <label htmlFor="psw">
            <b>Password</b>
          </label>
          <input
            type="password"
            placeholder="Enter Password"
            name="psw"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />

          <button type="submit" className="btn">
            Log In
          </button>

          {/* Modal for error message */}
          <Modal isOpen={isModalOpen} className='modal-content' onRequestClose={() => setIsModalOpen(false)}>
            <h2>Error</h2>
            <p>{errorMessage}</p>
            <button onClick={() => setIsModalOpen(false)}>Close</button>
          </Modal>

        </form>

        <p>
          Don't have an account?<Link to="/signup">Create an account</Link>
        </p>
      </div>
    </div>
  );
}

export default LoginScreen;
