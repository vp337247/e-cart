import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { createOrder } from '../redux/action/orderActions'
import '../sass/CheckoutScreen.scss';

function CheckoutScreen() {
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;

  const getCartSubTotal = () => {
    return cartItems.reduce((price, item) => item.price * item.qty + price, 0);
  };

  const [fullName, setFullName] = useState('');
  const [address, setAddress] = useState('');
  const [city, setCity] = useState('');
  const [postalCode, setPostalCode] = useState('');
  const [country, setCountry] = useState('');

  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState('creditCard'); 


  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handlePlaceOrder = async () => {
    try {
      // Prepare the order data
      const orderData = {
        fullName,
        address,
        city,
        postalCode,
        country,
        items: cartItems,
        subtotal: getCartSubTotal(),
      };

      if (selectedPaymentMethod === 'cod') {
        // Dispatch the createOrder action only if the payment method is "Cash on Delivery"
        dispatch(createOrder(orderData));
      }
      // Send the order data to the server
      const response = await axios.post('http://localhost:5000/api/orders', orderData);

      if (response.status === 200) {
        console.log('Order placed successfully');

        // Clear the cart items
        // You may need to dispatch an action to clear the cart items in the Redux store

        // Navigate to the OrderScreen
        navigate('/orders');
      }
    } catch (error) {
      console.error('Error placing order:', error);
    }
  };



  return (
    <div className="checkoutscreen">
      <div className="checkoutscreen__left">
        <h2>Delivery Address</h2>
        <form>
          <div className="form-group">
            <label htmlFor="fullName">Full Name</label>
            <input
              type="text"
              id="fullName"
              placeholder="Enter your full name"
              value={fullName}
              onChange={(e) => setFullName(e.target.value)}
            />

          </div>
          <div className="form-group">
            <label htmlFor="address">Address</label>
            <input
              type="text"
              id="address"
              placeholder="Enter your address"
              value={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="city">City</label>
            <input
              type="text"
              id="city"
              placeholder="Enter your city"
              value={city}
              onChange={(e) => setCity(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="postalCode">Postal Code</label>
            <input
              type="text"
              id="postalCode"
              placeholder="Enter your postal code"
              value={postalCode}
              onChange={(e) => setPostalCode(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="country">Country</label>
            <input
              type="text"
              id="country"
              placeholder="Enter your country"
              value={country}
              onChange={(e) => setCountry(e.target.value)}
            />
          </div>
        </form>
      </div>
      <div className="checkoutscreen__right">
        <div className="checkoutscreen__summary">
          <h2>Order Summary</h2>
          {cartItems.length === 0 ? (
            <div>
              <h3>Your Cart is Empty</h3>
              <Link to="/">Go Back</Link>
            </div>
          ) : (
            <div>
              {cartItems.map((item) => (
                <div key={item.product}>
                  <p>{item.name}</p>
                  <p>
                    {item.qty} x ${item.price.toFixed(2)}
                  </p>
                </div>
              ))}
            </div>
          )}
          <hr />
          <div className="checkoutscreen__subtotal">
            <h2>Subtotal ({cartItems.length}) items</h2>
            <h2>${getCartSubTotal().toFixed(2)}</h2>
          </div>
          <div className="checkoutscreen__payment">
            <h2>Payment Method</h2>
            <div className="payment-method-options">
              <label>
                <input
                  type="radio"
                  name="paymentMethod"
                  value="creditCard"
                  checked={selectedPaymentMethod === 'creditCard'}
                  onChange={() => setSelectedPaymentMethod('creditCard')}
                />
                Credit or Debit Card
              </label>
              <label>
                <input
                  type="radio"
                  name="paymentMethod"
                  value="cod"
                  checked={selectedPaymentMethod === 'cod'}
                  onChange={() => setSelectedPaymentMethod('cod')}
                />
                Cash on Delivery
              </label>



            </div>
          </div>
          <div className="checkoutscreen__placeorder">

            <button onClick={handlePlaceOrder}>Place Order</button>

          </div>
        </div>
      </div>
    </div>
  );
}

export default CheckoutScreen;
