import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '../sass/OrderScreen.scss';

function OrderScreen() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    const fetchOrders = async () => {
      try {
        const response = await axios.get('http://localhost:5000/api/orders/orders');
        setOrders(response.data);
      } catch (error) {
        console.error('Error fetching orders:', error);
      }
    };
  
    fetchOrders();
  }, []);
  

  const handleCancelOrder = (orderId) => {
    // Cancel order logic here
    console.log(`Cancel order with ID: ${orderId}`);
  };

  return (
    <div className="order-screen">
      <h1>Your Orders</h1>
      {orders.length === 0 ? (
        <div>
          <h3>No orders found</h3>
          <Link to="/">Go Back</Link>
        </div>
      ) : (
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Address</th>
              <th>Order Summary</th>
              <th>Subtotal</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {orders.map((order, index) => (
              <tr key={order._id}>
                <td>{index + 1}</td>
                <td>
                  {order.address.fullName}, {order.address.address}, {order.address.city}, {order.address.postalCode}, {order.address.country}
                </td>
                <td>{order.items.map((item) => item.product).join(', ')}</td>
                <td>${order.total.toFixed(2)}</td>
                <td>
                  <button onClick={() => handleCancelOrder(order._id)}>Cancel Order</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
}

export default OrderScreen;
