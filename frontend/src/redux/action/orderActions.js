import * as actionTypes from '../constants/orderConstants';
import axios from 'axios';

export const createOrder = (order) => async (dispatch, getState) => {
  try {
    dispatch({ type: actionTypes.CREATE_ORDER_REQUEST });

    const { data } = await axios.post('/api/orders', order);

    dispatch({
      type: actionTypes.CREATE_ORDER_SUCCESS,
      payload: data,
    });

    // Clear the cart items from local storage
    localStorage.removeItem('cart');
    // Clear the cart items from the Redux store
    //dispatch({ type: actionTypes.CLEAR_CART });
  } catch (error) {
    dispatch({
      type: actionTypes.CREATE_ORDER_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const getOrders = () => async (dispatch, getState) => {
  try {
    dispatch({ type: actionTypes.GET_ORDERS_REQUEST });

    const { data } = await axios.get('/api/orders');

    dispatch({
      type: actionTypes.GET_ORDERS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: actionTypes.GET_ORDERS_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
