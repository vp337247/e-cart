import * as actionTypes from "../constants/userConstants"

export const signupRequest = () => {
  return { type: actionTypes.SIGNUP_REQUEST };
};

export const signupSuccess = () => {
  return { type: actionTypes.SIGNUP_SUCCESS };
};

export const signupFailure = (error) => {
  return { type: actionTypes.SIGNUP_FAILURE, payload: error };
};

export const loginRequest = () => {
  return { type: actionTypes.LOGIN_REQUEST };
};

export const loginSuccess = () => {
  return { type: actionTypes.LOGIN_SUCCESS };
};

export const loginFailure = (error) => {
  return { type: actionTypes.LOGIN_FAILURE, payload: error };
};