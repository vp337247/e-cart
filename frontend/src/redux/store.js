import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

// Import your reducers
import { cartReducer } from './reducer/cartReducers';
import {
  getProductsReducer,
  getProductDetailsReducer,
} from './reducer/productReducers';
import {createOrderReducer,
    getOrdersReducer} from './reducer/orderReducers'

const rootReducer = combineReducers({
  cart: cartReducer,
  getProducts: getProductsReducer,
  getProductDetails: getProductDetailsReducer,
  createOrder: createOrderReducer,
  getOrder:getOrdersReducer,
});

const middleware = [thunk];

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
